# Base Image
FROM ubuntu:16.04

# Install Java
RUN apt-get update
RUN apt-get install -y software-properties-common python-software-properties debconf-utils
RUN add-apt-repository -y ppa:webupd8team/java
RUN apt-get update
RUN echo "oracle-java8-installer shared/accepted-oracle-license-v1-1 select true" | debconf-set-selections
RUN apt-get install -y oracle-java8-installer

# Create Project Folder
RUN mkdir -p /usr/javaproject

# Create Project Build Folder
RUN mkdir -p /usr/javaapp

# Copy HelloWorld.java
COPY HelloWorld.java /usr/javaproject

# Set working directory
WORKDIR /usr/javaapp

# Compile Java Files
RUN javac -d /usr/javaapp /usr/javaproject/*.java

# Create JAR file
RUN jar cvfe app.jar HelloWorld *.class

# Create mountable directories or volumes
VOLUME [ "/usr/javaproject", "/usr/javaapp" ]

# Execute app.jar
CMD [ "java", "-jar", "/usr/javaapp/app.jar" ]
